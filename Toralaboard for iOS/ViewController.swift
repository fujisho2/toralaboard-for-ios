//
//  ViewController.swift
//  Toralaboard for iOS
//
//  Created by fujisho on 2016/02/18.
//  Copyright © 2016年 Shotaro Fujie. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // Tableで使用する配列を定義する.
    private let profItems: NSArray = ["新谷", "大囿", "白松"]
    private let doctItems: NSArray = ["岩佐", "丹羽"]
    private let mast2Items: NSArray = ["小野田", "杉山", "中野"]
    private let mast1Items: NSArray = ["坂口", "杉野", "鈴木", "塚本", "寺澤", "野原", "藤江", "渡邊"]
    private let bach4Items: NSArray = ["伊藤（典）", "梅村", "大部", "芳賀", "日比野", "松浦", "村上", "山添"]
    private let bach3Items: NSArray = ["天野", "荒川", "伊藤（栄）", "岩田", "佐藤", "鹿田", "張", "両角"]
    
    // Sectionで使用する配列を定義する.
    private let mySections: NSArray = ["教員", "D", "M2", "M1", "B4", "B3"]
    
    private var who: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Status Barの高さを取得を.する.
        let barHeight: CGFloat = UIApplication.sharedApplication().statusBarFrame.size.height
        
        // Viewの高さと幅を取得する.
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height
        
        // TableViewの生成( status barの高さ分ずらして表示 ).
        let myTableView: UITableView = UITableView(frame: CGRect(x: 0, y: barHeight, width: displayWidth, height: displayHeight - barHeight))
        
        // Cell名の登録をおこなう.
        myTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        
        // DataSourceの設定をする.
        myTableView.dataSource = self
        
        // Delegateを設定する.
        myTableView.delegate = self
        
        // Viewに追加する.
        self.view.addSubview(myTableView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*
    セクションの数を返す.
    */
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return mySections.count
    }
    
    /*
    セクションのタイトルを返す.
    */
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return mySections[section] as? String
    }
    
    /*
    Cellが選択された際に呼び出される.
    */
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 0 {
            print("Value: \(profItems[indexPath.row])")
            who =  profItems[indexPath.row] as? String
            performSegueWithIdentifier("toSubViewController", sender: nil)
        } else if indexPath.section == 1 {
            print("Value: \(doctItems[indexPath.row])")
            who =  doctItems[indexPath.row] as? String
            performSegueWithIdentifier("toSubViewController", sender: nil)
        } else if indexPath.section == 2 {
            print("Value: \(mast2Items[indexPath.row])")
            who =  mast2Items[indexPath.row] as? String
            performSegueWithIdentifier("toSubViewController", sender: nil)
        } else if indexPath.section == 3 {
            print("Value: \(mast1Items[indexPath.row])")
            who =  mast1Items[indexPath.row] as? String
            performSegueWithIdentifier("toSubViewController", sender: nil)
        } else if indexPath.section == 4 {
            print("Value: \(bach4Items[indexPath.row])")
            who =  bach4Items[indexPath.row] as? String
            performSegueWithIdentifier("toSubViewController", sender: nil)
        } else if indexPath.section == 5 {
            print("Value: \(bach3Items[indexPath.row])")
            who =  bach3Items[indexPath.row] as? String
            performSegueWithIdentifier("toSubViewController", sender: nil)
        }
    }
    
    /*
    テーブルに表示する配列の総数を返す.
    */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return profItems.count
        } else if section == 1 {
            return doctItems.count
        } else if section == 2 {
            return mast2Items.count
        } else if section == 3 {
            return mast1Items.count
        } else if section == 4 {
            return bach4Items.count
        } else if section == 5 {
            return bach3Items.count
        } else {
            return 0
        }
    }
    
    /*
    Cellに値を設定する.
    */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("MyCell", forIndexPath: indexPath)
        
        if indexPath.section == 0 {
            cell.textLabel?.text = "\(profItems[indexPath.row])"
        } else if indexPath.section == 1 {
            cell.textLabel?.text = "\(doctItems[indexPath.row])"
        } else if indexPath.section == 2 {
            cell.textLabel?.text = "\(mast2Items[indexPath.row])"
        } else if indexPath.section == 3 {
            cell.textLabel?.text = "\(mast1Items[indexPath.row])"
        } else if indexPath.section == 4 {
            cell.textLabel?.text = "\(bach4Items[indexPath.row])"
        } else if indexPath.section == 5 {
            cell.textLabel?.text = "\(bach3Items[indexPath.row])"
        }
        
        return cell
    }

    /* 
    Segue
    */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "toSubViewController") {
            let subVC: SubViewController = (segue.destinationViewController as? SubViewController)!
            
            subVC.who = who
        }
    }

}

