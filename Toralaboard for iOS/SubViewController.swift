//
//  SubViewController.swift
//  Toralaboard for iOS
//
//  Created by fujisho on 2016/02/18.
//  Copyright © 2016年 Shotaro Fujie. All rights reserved.
//

import UIKit

class SubViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // Tableで使用する配列を定義する.
    private let laboItems: NSArray = ["教官室（新谷）", "教官室（大囿）", "教官室（白松）", "院部屋", "学部部屋", "ゼミ室", "秘書室", "学内", "図書館", "講義", "食事", "学外", "帰宅"]
//    private let onCamItems: NSArray = ["学内", "図書館", "講義", "食事"]
//    private let exCamItems: NSArray = ["学外", "食事"]
//    private let
    
    // Sectionで使用する配列を定義する.
    private let mySections: NSArray = ["居場所"]
    
    var who: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Status Barの高さを取得を.する.
        let barHeight: CGFloat = UIApplication.sharedApplication().statusBarFrame.size.height
        
        // Viewの高さと幅を取得する.
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height
        
        // TableViewの生成( status barの高さ分ずらして表示 ).
        let myTableView: UITableView = UITableView(frame: CGRect(x: 0, y: barHeight, width: displayWidth, height: displayHeight - barHeight))
        
        // Cell名の登録をおこなう.
        myTableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        
        // DataSourceの設定をする.
        myTableView.dataSource = self
        
        // Delegateを設定する.
        myTableView.delegate = self
        
        // Viewに追加する.
        self.view.addSubview(myTableView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /*
    セクションの数を返す.
    */
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return mySections.count
    }
    
    /*
    セクションのタイトルを返す.
    */
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return mySections[section] as? String
    }
    
    /*
    Cellが選択された際に呼び出される.
    */
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 0 {
            print("Value: \(laboItems[indexPath.row])")
            
            let alertController = UIAlertController(title: "\(who!)", message: "\(laboItems[indexPath.row])に移動しました．", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .Default) { action in
                self.dismissViewControllerAnimated(true, completion: nil)
            }
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)
            
            
        }
//        } else if indexPath.section == 1 {
//            print("Value: \(onCamItems[indexPath.row])")
//        }
    }
    
    /*
    テーブルに表示する配列の総数を返す.
    */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return laboItems.count
        } /*else if section == 1 {
            return onCamItems.count
        } */else {
            return 0
        }
    }
    
    /*
    Cellに値を設定する.
    */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("MyCell", forIndexPath: indexPath)
        
        if indexPath.section == 0 {
            cell.textLabel?.text = "\(laboItems[indexPath.row])"
        }
//        else if indexPath.section == 1 {
//            cell.textLabel?.text = "\(onCamItems[indexPath.row])"
//        }
        
        return cell
    }
}
